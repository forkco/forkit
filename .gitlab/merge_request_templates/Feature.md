## Summary

*Please include a summary of the change and which issue is fixed. Please also include relevant motivation and context. List any dependencies that are required for this change.*

- What changed and why?
- What does it fix?
- How has it been tested?

## Reviewers

Tag someone to review your code. Since we'll be working as smaller teams, you must tag at least everyone on your team.

@manuel @zezinho @tone

____

# Useful links
[redmine](http://mivbox.di.uminho.pt:9000/issues/7587)

----

**Estimated time spent:** 2 horas
