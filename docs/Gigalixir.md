# Deploy 101

## Gigalixir
Firstly make sure you have the Gigalixir CLI installed: `sudo pip install gigalixir --ignore-installed six`

### Sign up and log in
`gigalixir signup`
`gigalixir login`

### Deploy an app
From master: `git push gigalixir master`
From another branch: `git push gigalixir <branch-name>:master`

### Setup git remote
`git remote add gigalixir <repository-name>.git`

### List apps
`gigalixir apps`

### List postgres dabatases
`gigalixir pg`

### Check your app status
`gigalixir ps`

### Check logs
`gigalixir logs`

### Add ssh key
`gigalixir account:ssh_keys:add "$(cat ~/.ssh/id_rsa.pub)"`

### Remote console
`gigalixir ps:remote_console`

### Launch a remote observer
`gigalixir ps:observer`

### Run distillery commands
`gigalixir ps:distillery $COMMAND`

### Run migrations (with distillery)
`gigalixir ps:migrate`





