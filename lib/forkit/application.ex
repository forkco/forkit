defmodule Forkit.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      Forkit.Repo,
      # Start the endpoint when the application starts
      ForkitWeb.Endpoint
      # Starts a worker by calling: Forkit.Worker.start_link(arg)
      # {Forkit.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Forkit.Supervisor]
    res = Supervisor.start_link(children, opts)
    if Mix.env() == :prod do 
      setup_database()
    end
    res
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ForkitWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp setup_database() do
    drop_database()
    run_migration()
    run_seeds()
  end

  defp drop_database() do
    IO.puts "Dropping database - WARNING: this shouldn't be used in production!"
    Ecto.Migrator.run(Forkit.Repo, Path.join(["#{:code.priv_dir(:forkit)}", "repo", "migrations"]), :down, all: true)
  end

  defp run_migration() do
    IO.puts "Running migrations"
    Ecto.Migrator.run(Forkit.Repo, Path.join(["#{:code.priv_dir(:forkit)}", "repo", "migrations"]), :up, all: true)
  end

  defp run_seeds() do
    IO.puts "Running seeds"
    Code.eval_file(priv_path_for(Forkit.Repo, "seeds.exs"))
  end

  defp priv_path_for(repo, filename) do
    app = Keyword.get(repo.config, :otp_app)
    repo_underscore = repo |> Module.split |> List.last |> Macro.underscore
    Path.join(["#{:code.priv_dir(app)}", repo_underscore, filename])
  end
end
