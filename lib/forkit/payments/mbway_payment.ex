defmodule Forkit.Payments.MbwayPayment do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Payments.Payment

  schema "mbway_payments" do
    field :token, :string
    field :merchant_id, :integer
    field :confirmed, :boolean, default: false

    belongs_to :payment, Payment

    timestamps()
  end

  def create_changeset(mbway, attrs) do
    mbway
    |> cast(attrs, [:merchant_id, :token])
    |> validate_required([:merchant_id])
  end

  def update_changeset(mbway, attrs) do
    mbway
    |> cast(attrs, [:confirmed])
    |> validate_acceptance(:confirmed)
  end

  def create_message(payment) do
    payload = %{
      amount: 3,
      operationTypeCode: "022",
      merchantID: "123123",
      endpoint: "forkittest.gigalixirapp.com/api/mbway/payment-confirmation"
    }

    Poison.encode!(payload)
  end
end
