defmodule Forkit.Payments.Payment do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Payments.{MbwayPayment, UtrustPayment}

  schema "payments" do
    field :restaurant_id, :integer
    field :order_id, :integer
    field :amount, :integer

    has_one :utrust_payment, UtrustPayment

    timestamps()
  end

  def create_changeset(payment, attrs) do
    payment
    |> cast(attrs, [:restaurant_id, :order_id, :amount])
    |> cast_assoc(:utrust_payment, with: &UtrustPayment.create_changeset/2)
    |> validate_required([:restaurant_id, :order_id, :amount])
  end
end
