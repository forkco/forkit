defmodule Forkit.Payments do
  import Ecto.Query, warn: false

  alias Forkit.Repo
  alias Forkit.Payments.{Payment, MbwayPayment, UtrustPayment}
  alias Forkit.Restaurants.Order

  def list_payments do
    Payment
    |> Repo.all()
    |> Repo.preload([:mbway_payment, :utrust_payment])
  end

  def pay_with_mbway(attrs \\ %{}) do
    message = MbwayPayment.create_message(attrs)

    response = HTTPoison.post(
      "forkittest.herokuapp.com/api/payments",
      message,
      [{"Content-Type", "application/json"}]
    )

    case response do
      {:ok, %HTTPoison.Response{status_code: 200, body: payment_token}} ->
        body = Poison.decode!(payment_token)
        attrs = Map.update!(attrs, "mbway_payment", &Map.put(&1, "token", body["token"]))
        create_payment(attrs)
      _ ->
        {:error, :mbway_error}
    end
  end

  def pay_with_utrust(attrs \\ %{}) do
    list_of_products = get_list_of_products(attrs)
    message = UtrustPayment.create_message(attrs, list_of_products)

    response = HTTPoison.post(
      "https://merchants.api.pixels-utrust.com/api/stores/orders",
      message,
      utrust_request_header()
    )

    case response do
      {:ok, %HTTPoison.Response{status_code: 201, body: data}} ->
        data = Poison.decode!(data)
        attrs = Map.update!(attrs, :utrust_payment, &Map.put(&1, :token, data["data"]["id"]))
        create_payment(attrs)

        {:ok, data["data"]["attributes"]["redirect_url"]}
      _ ->
        {:error, :utrust_error}
    end
  end

  def pay_with_utrust_2(attrs \\ %{}) do
    list_of_products = get_list_of_products_lower(attrs)
    message = UtrustPayment.create_message(attrs, list_of_products)

    response = HTTPoison.post(
      "https://merchants.api.pixels-utrust.com/api/stores/orders",
      message,
      utrust_request_header()
    )

    case response do
      {:ok, %HTTPoison.Response{status_code: 201, body: data}} ->
        data = Poison.decode!(data)
        attrs = Map.update!(attrs, :utrust_payment, &Map.put(&1, :token, data["data"]["id"]))
        create_payment(attrs)

        {:ok, data["data"]["attributes"]["redirect_url"]}
      _ ->
        {:error, :utrust_error}
    end
  end

  def confirm_mbway_payment(token, confirmation) do
    mbway =
      MbwayPayment
      |> Repo.get_by(token: token)
      |> update_mbway_payment(%{confirmed: confirmation})

    {:ok, mbway}
  end

  def confirm_utrust_payment(token, confirmation) do
    utrust =
      UtrustPayment
      |> Repo.get_by(token: token)
      |> update_utrust_payment(%{confirmed: confirmation})

    {:ok, utrust}
  end

  def cancel_utrust_payment(token) do
    UtrustPayment
    |> Repo.get_by(token: token)
    |> Repo.delete
  end

  defp create_payment(attrs) do
    %Payment{}
    |> Payment.create_changeset(attrs)
    |> Repo.insert()
  end

  defp update_mbway_payment(%MbwayPayment{} = payment, attrs) do
    payment
    |> MbwayPayment.update_changeset(attrs)
    |> Repo.update()
  end

  defp update_utrust_payment(%UtrustPayment{} = payment, attrs) do
    payment
    |> UtrustPayment.update_changeset(attrs)
    |> Repo.update()
  end

  defp get_list_of_products(payment) do
    order = Order
    |> Repo.get(payment.order_id)
    |> Repo.preload([requests: [:product]])

    order.requests
    |> Enum.map(&filter_request_params(&1))
  end

  defp get_list_of_products_lower(payment) do
    order = Order
    |> Repo.get(payment.order_id)
    |> Repo.preload([requests: [:product]])

    order.requests
    |> Enum.map(&low_params(&1))
  end

  defp filter_request_params(request_params) do
    %{
      sku: request_params.product.name,
      name: request_params.product.name,
      price: Float.to_string(request_params.product.price / 100),
      currency: "EUR",
      quantity: 1
    }
  end

  defp low_params(request_params) do
    %{
      sku: request_params.product.name,
      name: request_params.product.name,
      price: "1.0",
      currency: "EUR",
      quantity: 1
    }
  end

  defp utrust_request_header() do
    token = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJtZXJjaGFudF9jbGllbnQiLCJleHAiOjE1NTU2MTYwNTMsImlhdCI6MTU1MzE5Njg1MywiaXNzIjoibWVyY2hhbnRfY2xpZW50IiwianRpIjoiN2JlMjA5YjYtZjdmMS00ZGRlLTk4YjQtZTBmNWIwODM4YmI2IiwibmJmIjoxNTUzMTk2ODUyLCJzdWIiOiJTdG9yZTpjZDkxOTcxYi03NzNhLTRiM2MtYjdlYi1iYmJhOWVhOGI3YWYiLCJ0eXAiOiJhY2Nlc3MifQ.EmIr3U_FiZFJ5x6JohhwjQdg2-28EOHP3NtpWJvLHJ6crRHzwaRTgPbh1_8M56J-PxJy_MvUnnJYGzABDJxkfg"
    [Authorization: "Bearer #{token}", "Content-Type": "application/json"]
  end
end
