defmodule Forkit.Payments.UtrustPayment do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Payments.Payment

  schema "utrust_payments" do
    field :callback_url, :string
    field :cancel_url, :string
    field :return_url, :string
    field :token, :string
    field :confirmed, :boolean, default: false

    belongs_to :payment, Payment

    timestamps()
  end

  def create_changeset(utrust, attrs) do
    utrust
    |> cast(attrs, [:confirmed, :token, :return_url, :cancel_url, :callback_url])
    |> validate_required([:token])
  end

  @doc false
  def update_changeset(utrust_payment, attrs) do
    utrust_payment
    |> cast(attrs, [:confirmed])
    |> validate_required([:confirmed])
  end

  def create_message(payment, list_of_products) do
    order_id = Integer.to_string(payment.order_id)
    amount = Float.to_string(payment.amount / 100)

    payload = %{
      data: %{
        type: "orders",
        attributes: %{
          order: %{
            reference: order_id,
            amount: %{
              total: amount,
              currency: "EUR",
              details: %{
                subtotal: amount,
                handling_fee: "0.0"
              }
            },
            return_urls: %{
              return_url: "https://forkittest.gigalixirapp.com/api/utrust/payment-confirmation",
              cancel_url: "https://forkittest.gigalixirapp.com/api/utrust/payment-cancelation",
              callback_url: "https://forkittest.gigalixirapp.com/api/utrust/payment-received"
            },
            line_items: list_of_products
          },
          customer: %{
            first_name: payment.utrust.first_name,
            last_name: payment.utrust.last_name,
            email: payment.utrust.email,
            country: "PT"
          }
        }
      }
    }

    Poison.encode!(payload)
  end
end
