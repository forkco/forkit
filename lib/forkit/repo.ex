defmodule Forkit.Repo do
  use Ecto.Repo,
    otp_app: :forkit,
    adapter: Ecto.Adapters.Postgres
end
