defmodule Forkit.Restaurants.Address do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.Restaurant

  schema "addresses" do 
    field :street, :string
    field :city, :string
    field :country, :string

    belongs_to :restaurant, Restaurant
  end

  def changeset(address, attrs) do
    address
    |> cast(attrs, [:street, :city, :country])
    |> validate_required([:street, :city, :country])
    |> assoc_constraint(:restaurant)
  end
end
