defmodule Forkit.Restaurants.Category do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.{Product, Restaurant}

  schema "categories" do
    field :name, :string
    field :image, :string

    belongs_to :restaurant, Restaurant
    has_many :products, Product

    timestamps()
  end

  @doc false
  def changeset(category, attrs) do
    category
    |> cast(attrs, [:name, :image, :restaurant_id])
    |> validate_required([:name, :restaurant_id])
    |> assoc_constraint(:restaurant)
  end
end
