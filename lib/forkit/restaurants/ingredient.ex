defmodule Forkit.Restaurants.Ingredient do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.{Product, ProductIngredient}

  schema "ingredients" do
    field :name, :string

    many_to_many :products, Product, join_through: ProductIngredient

    timestamps()
  end

  @doc false
  def changeset(ingredient, attrs) do
    ingredient
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
