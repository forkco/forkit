defmodule Forkit.Restaurants.Order do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.{ProductRequest, Restaurant}

  schema "orders" do
    field :note, :string
    field :status, :integer, default: 0 # 0: Submitted, 1: Processing, 2: Ready
    field :type, :integer # 0 : Normal, 1 : Take-away

    has_many :requests, ProductRequest
    belongs_to :restaurant, Restaurant

    timestamps()
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [:status, :type, :note, :restaurant_id])
    |> cast_assoc(:requests)
    |> validate_required([:status, :type, :requests, :restaurant_id])
    |> validate_inclusion(:status, 0..2)
    |> validate_inclusion(:type, 0..1)
    |> assoc_constraint(:restaurant)
  end

  @doc false
  def update_changeset(order, attrs) do
    order
    |> cast(attrs, [:status])
    |> validate_required([:status])
    |> validate_inclusion(:status, 0..2)
  end
end
