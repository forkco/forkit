defmodule Forkit.Restaurants.Product do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.{
    Restaurant,
    Ingredient, 
    ProductIngredient, 
    Category
  }

  schema "products" do
    field :available, :boolean, default: true
    field :description, :string
    field :image, :string
    field :name, :string
    field :price, :integer

    belongs_to :category, Category
    belongs_to :restaurant, Restaurant
    many_to_many :ingredients, Ingredient, join_through: ProductIngredient

    timestamps()
  end

  @doc false
  def create_changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :image, :price, :description, :available, :restaurant_id, :category_id])
    |> validate_required([:name, :price, :restaurant_id, :category_id])
    |> assoc_constraint(:restaurant)
    |> assoc_constraint(:category)
    |> unique_constraint(:name)
  end

  @doc false
  def update_changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :image, :price, :description, :available])
    |> validate_required([:name, :price])
    |> assoc_constraint(:restaurant)
    |> assoc_constraint(:category)
    |> unique_constraint(:name)
  end
end
