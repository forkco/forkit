defmodule Forkit.Restaurants.ProductIngredient do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.{Product, Ingredient}

  schema "products_ingredients" do
    belongs_to :product, Product
    belongs_to :ingredient, Ingredient

    timestamps()
  end

  @doc false
  def changeset(product_ingredient, attrs) do
    product_ingredient
    |> cast(attrs, [:product_id, :ingredient_id])
    |> validate_required([:product_id, :ingredient_id])
    |> assoc_constraint(:product)
    |> assoc_constraint(:ingredient)
  end
end
