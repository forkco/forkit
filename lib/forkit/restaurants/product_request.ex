defmodule Forkit.Restaurants.ProductRequest do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.{Ingredient, Product, ProductRequestIngredient, Order}

  schema "product_requests" do
    field :note, :string

    belongs_to :order, Order
    belongs_to :product, Product
    many_to_many :ingredients, Ingredient, join_through: ProductRequestIngredient

    timestamps()
  end

  @doc false
  def changeset(product_request, attrs) do
    product_request
    |> cast(attrs, [:note, :product_id, :order_id])
    |> validate_required([:product_id])
    |> assoc_constraint(:product)
    |> assoc_constraint(:order)
  end
end
