defmodule Forkit.Restaurants.ProductRequestIngredient do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.{ProductRequest, Ingredient}

  schema "product_requests_ingredients" do
    belongs_to :product_request, ProductRequest
    belongs_to :ingredient, Ingredient

    timestamps()
  end

  @doc false
  def changeset(product_request_ingredient, attrs) do
    product_request_ingredient
    |> cast(attrs, [:product_request_id, :ingredient_id])
    |> validate_required([:product_request_id, :ingredient_id])
    |> assoc_constraint(:ingredient)
    |> assoc_constraint(:product_request)
  end
end
