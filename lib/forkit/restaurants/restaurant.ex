defmodule Forkit.Restaurants.Restaurant do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.{
    Category, 
    Product,
    Address, 
    Schedule
  }

  schema "restaurants" do
    field :name, :string
  
    has_one :schedule, Schedule, on_replace: :update, on_delete: :delete_all
    has_one :address, Address, on_replace: :update, on_delete: :delete_all

    has_many :categories, Category
    has_many :products, Product

    timestamps()
  end

  @doc false
  def changeset(restaurant, attrs) do
    restaurant
    |> cast(attrs, [:name])
    |> cast_assoc(:address, with: &Address.changeset/2)
    |> cast_assoc(:schedule, with: &Schedule.changeset/2)
    |> validate_required([:name, :address, :schedule])
    |> unique_constraint(:name)
  end
end
