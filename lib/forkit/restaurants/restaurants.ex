defmodule Forkit.Restaurants do
  import Ecto.Query, warn: false

  alias Forkit.Repo
  alias Forkit.Restaurants.{
    Restaurant,
    Product,
    Category,
    Ingredient,
    Order
  }

  def list_restaurants do
    Restaurant
    |> Repo.all()
    |> Repo.preload([:schedule, :address])
  end

  def get_restaurant!(id) do
    Restaurant
    |> Repo.get!(id)
    |> Repo.preload([:schedule, :address])
  end

  def create_restaurant(attrs \\ %{}) do
    with {:ok, restaurant} <-
      %Restaurant{}
      |> Restaurant.changeset(attrs)
      |> Repo.insert()
    do
      {:ok, restaurant |> Repo.preload([:schedule, :address])}
    end
  end

  def update_restaurant(%Restaurant{} = restaurant, attrs) do
    restaurant
    |> Restaurant.changeset(attrs)
    |> Repo.update()
  end

  def delete_restaurant(%Restaurant{} = restaurant) do
    Repo.delete(restaurant)
  end

  def get_product!(%Restaurant{} = restaurant, id) do
    Product
    |> where(restaurant_id: ^restaurant.id)
    |> Repo.get!(id)
    |> Repo.preload(:ingredients)
  end

  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.create_changeset(attrs)
    |> Repo.insert()
  end

  def update_product(%Product{} = product, attrs) do
    product
    |> Product.update_changeset(attrs)
    |> Repo.update()
  end

  def delete_product(%Product{} = product) do
    Repo.delete(product)
  end

  def list_ingredients do
    Repo.all(Ingredient)
  end

  def get_ingredient!(id) do
    Repo.get!(Ingredient, id)
  end

  def create_ingredient(attrs \\ %{}) do
    %Ingredient{}
    |> Ingredient.changeset(attrs)
    |> Repo.insert()
  end

  def update_ingredient(%Ingredient{} = ingredient, attrs) do
    ingredient
    |> Ingredient.changeset(attrs)
    |> Repo.update()
  end

  def delete_ingredient(%Ingredient{} = ingredient) do
    Repo.delete(ingredient)
  end

  def list_orders(%Restaurant{} = restaurant) do
    Order
    |> where(restaurant_id: ^restaurant.id)
    |> Repo.all
    |> Repo.preload([requests: [:ingredients, :product]])
  end

  def get_order!(%Restaurant{} = restaurant, id) do
    Order
    |> where(restaurant_id: ^restaurant.id)
    |> Repo.get!(id)
    |> Repo.preload([requests: [:ingredients, :product]])
  end

  def create_order(attrs \\ %{}) do
    with {:ok, order} <-
      %Order{}
      |> Order.changeset(attrs)
      |> Repo.insert()
    do
      {:ok, order |> Repo.preload([requests: [:ingredients, :product]])}
    end
  end

  def update_order(%Order{} = order, attrs) do
    order
    |> Order.update_changeset(attrs)
    |> Repo.update()
  end

  def list_categories(%Restaurant{} = restaurant) do
    Category
    |> where(restaurant_id: ^restaurant.id)
    |> Repo.all()
    |> Repo.preload(products: :ingredients)
  end

  def get_category!(%Restaurant{} = restaurant, id) do
    Category
    |> where(restaurant_id: ^restaurant.id)
    |> Repo.get!(id)
    |> Repo.preload(products: :ingredients)
  end

  def create_category(attrs \\ %{}) do
    with {:ok, category} <-
      %Category{}
      |> Category.changeset(attrs)
      |> Repo.insert()
    do
      {:ok, category |> Repo.preload(:products)}
    end
  end

  def update_category(%Category{} = category, attrs) do
    category
    |> Category.changeset(attrs)
    |> Repo.update()
  end

  def delete_category(%Category{} = category) do
    Repo.delete(category)
  end
end
