defmodule Forkit.Restaurants.Schedule do
  use Ecto.Schema
  import Ecto.Changeset

  alias Forkit.Restaurants.Restaurant

  schema "schedules" do 
    field :open_at, :time
    field :close_at, :time

    belongs_to :restaurant, Restaurant
  end

  def changeset(schedule, attrs) do
    schedule
    |> cast(attrs, [:open_at, :close_at])
    |> validate_required([:open_at, :close_at])
    |> assoc_constraint(:restaurant)
  end
end
