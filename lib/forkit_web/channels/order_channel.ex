defmodule ForkitWeb.OrderChannel do
  use Phoenix.Channel

  def broadcast_order_update(order) do
    channel = "orders:#{order.id}"
    payload = %{
      id: order.id,
      status: order.status
    }

    ForkitWeb.Endpoint.broadcast(channel, "status_updated", payload)
  end

  def join("orders:" <> _order_id, _params, socket) do
    {:ok, socket}
  end

  def handle_in("status_updated", payload, socket) do
    broadcast!(socket, "status_updated", payload)

    {:noreply, socket}
  end
end
