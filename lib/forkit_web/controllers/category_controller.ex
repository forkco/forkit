defmodule ForkitWeb.CategoryController do
  use ForkitWeb, :controller

  alias Forkit.Restaurants
  alias Forkit.Restaurants.Category

  action_fallback ForkitWeb.FallbackController

  plug :assigns_restaurant

  def action(conn, _) do
    args = [conn, conn.params, conn.assigns.restaurant]
    apply(__MODULE__, action_name(conn), args)
  end

  def index(conn, _params, restaurant) do
    categories = Restaurants.list_categories(restaurant)
    render(conn, :index, categories: categories)
  end

  def create(conn, %{"category" => category_params}, restaurant) do
    category_params = category_params |> Map.put("restaurant_id", restaurant.id)

    with {:ok, %Category{} = category} <- Restaurants.create_category(category_params) do
      conn
      |> put_status(:created)
      |> render(:show, category: category)
    end
  end

  def update(conn, %{"id" => id, "category" => category_params}, restaurant) do
    updated_category =
      Restaurants.get_category!(restaurant, id)
      |> Restaurants.update_category(category_params)

    with {:ok, %Category{} = category} <- updated_category do
      render(conn, :show, category: category)
    end
  end

  def delete(conn, %{"id" => id}, restaurant) do
    category = Restaurants.get_category!(restaurant, id)

    with {:ok, %Category{}} <- Restaurants.delete_category(category) do
      send_resp(conn, :no_content, "")
    end
  end

  defp assigns_restaurant(conn, _) do
    restaurant = Restaurants.get_restaurant!(conn.params["restaurant_id"])
    assign(conn, :restaurant, restaurant)
  end
end
