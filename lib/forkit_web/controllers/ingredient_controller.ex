defmodule ForkitWeb.IngredientController do
  use ForkitWeb, :controller

  alias Forkit.Restaurants
  alias Forkit.Restaurants.Ingredient

  action_fallback ForkitWeb.FallbackController

  def index(conn, _params) do
    ingredients = Restaurants.list_ingredients()
    render(conn, :index, ingredients: ingredients)
  end

  def create(conn, %{"ingredient" => ingredient_params}) do
    with {:ok, %Ingredient{} = ingredient} <- Restaurants.create_ingredient(ingredient_params) do
      conn
      |> put_status(:created)
      |> render(:show, ingredient: ingredient)
    end
  end

  def update(conn, %{"id" => id, "ingredient" => ingredient_params}) do
    result =
      Restaurants.get_ingredient!(id)
      |> Restaurants.update_ingredient(ingredient_params)

    with {:ok, %Ingredient{} = ingredient} <- result do
      conn
      |> render(:show, ingredient: ingredient)
    end
  end

  def delete(conn, %{"id" => id}) do
    ingredient = Restaurants.get_ingredient!(id)

    with {:ok, %Ingredient{}} <- Restaurants.delete_ingredient(ingredient) do
      send_resp(conn, :no_content, "")
    end
  end
end
