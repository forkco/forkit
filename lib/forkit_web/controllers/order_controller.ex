defmodule ForkitWeb.OrderController do
  use ForkitWeb, :controller

  alias Forkit.Restaurants
  alias Forkit.Restaurants.Order
  alias ForkitWeb.OrderChannel

  action_fallback ForkitWeb.FallbackController

  plug :assigns_restaurant

  def action(conn, _) do
    args = [conn, conn.params, conn.assigns.restaurant]
    apply(__MODULE__, action_name(conn), args)
  end

  def index(conn, _params, restaurant) do
    orders = Restaurants.list_orders(restaurant)
    render(conn, "index.json", orders: orders)
  end

  def create(conn, %{"order" => order_params}, restaurant) do
    order_params = Map.put(order_params, "restaurant_id", restaurant.id)

    with {:ok, %Order{} = order} <- Restaurants.create_order(order_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.restaurant_order_path(conn, :show, restaurant, order))
      |> render("show.json", order: order)
    end
  end

  def show(conn, %{"id" => id}, restaurant) do
    order = Restaurants.get_order!(restaurant, id)
    render(conn, "show.json", order: order)
  end

  def update(conn, %{"id" => id, "order" => order_params}, restaurant) do
    order = Restaurants.get_order!(restaurant, id)

    with {:ok, %Order{} = order} <- Restaurants.update_order(order, order_params) do
      OrderChannel.broadcast_order_update(order)
      render(conn, "show.json", order: order)
    end
  end

  defp assigns_restaurant(conn, _) do
    restaurant = Restaurants.get_restaurant!(conn.params["restaurant_id"])
    assign(conn, :restaurant, restaurant)
  end
end
