defmodule ForkitWeb.PaymentController do
  use ForkitWeb, :controller

  action_fallback ForkitWeb.FallbackController

  alias Forkit.Payments

  def create(conn, %{"payment" => payment_params}) do
    payment_params = Map.put(payment_params, "restaurant_id", conn.params["restaurant_id"])
    payment_params = Map.put(payment_params, "order_id", conn.params["order_id"])
    result = Payments.pay_with_utrust(payment_params)

    case result do
      {:ok, redirect_url} ->
        IO.puts(redirect_url)
        redirect(conn, external: redirect_url)
      _ ->
        {:error, :utrust_error}
    end
  end

  def high_test_api(conn, params) do
    payment_params = %{
      amount: params["payments"]["amount"],
      utrust_payment: %{
        confirmed: false,
      },
      utrust: %{
        first_name: params["payments"]["utrust_payment"]["first_name"],
        last_name: params["payments"]["utrust_payment"]["last_name"],
        email: params["payments"]["utrust_payment"]["email"],
      },
      restaurant_id: 1,
      order_id: 2,
    }
    result = Payments.pay_with_utrust(payment_params)

    case result do
      {:ok, redirect_url} ->
        IO.puts(redirect_url)
        redirect(conn, external: redirect_url)
      _ ->
        {:error, :utrust_error}
    end
  end

  def test_api(conn, _params) do
    payment_params = %{
      amount: 500,
      utrust_payment: %{
        confirmed: false
      },
      restaurant_id: 1,
      order_id: 2
    }
    result = Payments.pay_with_utrust(payment_params)

    case result do
      {:ok, redirect_url} ->
        IO.puts(redirect_url)
        redirect(conn, external: redirect_url)
      _ ->
        {:error, :utrust_error}
    end
  end

  def low_test_api(conn, _params) do
    payment_params = %{
      amount: 100,
      utrust_payment: %{
        confirmed: false
      },
      restaurant_id: 1,
      order_id: 2
    }
    result = Payments.pay_with_utrust_2(payment_params)

    case result do
      {:ok, redirect_url} ->
        IO.puts(redirect_url)
        redirect(conn, external: redirect_url)
      _ ->
        {:error, :utrust_error}
    end
  end

  def index(conn, _params) do
    payments = Payments.list_payments()
    render(conn, "index.json", payments: payments)
  end

  def mbway_callback(conn, %{"token" => token, "statusCode" => "000"}) do
    Payments.confirm_mbway_payment(token, true)
    conn |> text("ok")
  end

  def utrust_confirmation_callback(conn, _params) do
    IO.puts "confirmation_callback"
    # Payments.confirm_utrust_payment(data["token"], true)

    conn |> text("ok")
  end

  def utrust_cancelation_callback(conn, _data) do
    IO.puts "confirmation_callback"
    # IO.puts(data)
    # Payments.cancel_utrust_payment(data["token"])

    conn |> text("ok")
  end

  def utrust_received_callback(conn, _data) do
    IO.puts "confirmation_callback"
    # IO.puts(data)
    # Payments.confirm_utrust_payment(data["token"], true)
    IO.puts "Ze V."
    conn |> text("ok")
  end
end
