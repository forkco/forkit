defmodule ForkitWeb.ProductController do
  use ForkitWeb, :controller

  alias Forkit.Restaurants
  alias Forkit.Restaurants.Product

  action_fallback ForkitWeb.FallbackController

  plug :assigns_restaurant

  def action(conn, _) do
    args = [conn, conn.params, conn.assigns.restaurant]
    apply(__MODULE__, action_name(conn), args)
  end

  def create(conn, %{"product" => product_params}, restaurant) do
    product_params = product_params |> Map.put("restaurant_id", restaurant.id)    
    
    with {:ok, %Product{} = product} <- Restaurants.create_product(product_params) do
      conn
      |> put_status(:created)
      |> render(:show, product: product)
    end
  end

  def update(conn, %{"id" => id, "product" => product_params}, restaurant) do
    updated_product = 
      Restaurants.get_product!(restaurant, id)
      |> Restaurants.update_product(product_params)

    with {:ok, %Product{} = product} <- updated_product do
      render(conn, :show, product: product)
    end
  end

  def update(conn, %{"id" => id, "available" => availability}, restaurant) do
    updated_product = 
      Restaurants.get_product!(restaurant, id)
      |> Restaurants.update_product(%{available: availability})

    with {:ok, %Product{} = product} <- updated_product do
      render(conn, :show, product: product)
    end
  end

  def delete(conn, %{"id" => id}, restaurant) do
    product = Restaurants.get_product!(restaurant, id)

    with {:ok, %Product{}} <- Restaurants.delete_product(product) do
      send_resp(conn, :no_content, "")
    end
  end

  defp assigns_restaurant(conn, _) do
    restaurant = Restaurants.get_restaurant!(conn.params["restaurant_id"])
    assign(conn, :restaurant, restaurant)
  end
end
