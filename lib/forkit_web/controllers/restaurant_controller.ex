defmodule ForkitWeb.RestaurantController do
  use ForkitWeb, :controller

  alias Forkit.Restaurants
  alias Forkit.Restaurants.Restaurant

  action_fallback ForkitWeb.FallbackController

  def index(conn, _params) do
    restaurants = Restaurants.list_restaurants()
    render(conn, :index, restaurants: restaurants)
  end

  def create(conn, %{"restaurant" => restaurant_params}) do
    with {:ok, %Restaurant{} = restaurant} <- Restaurants.create_restaurant(restaurant_params) do
      conn
      |> put_status(:created)
      |> render(:show, restaurant: restaurant)
    end
  end

  def update(conn, %{"id" => id, "restaurant" => restaurant_params}) do
    result =
      Restaurants.get_restaurant!(id)
      |> Restaurants.update_restaurant(restaurant_params)

    with {:ok, %Restaurant{} = restaurant} <- result do
      conn
      |> render(:show, restaurant: restaurant)
    end
  end

  def delete(conn, %{"id" => id}) do
    restaurant = Restaurants.get_restaurant!(id)

    with {:ok, %Restaurant{}} <- Restaurants.delete_restaurant(restaurant) do
      send_resp(conn, :no_content, "")
    end
  end
end
