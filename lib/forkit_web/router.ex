defmodule ForkitWeb.Router do
  use ForkitWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ForkitWeb do
    pipe_through :api

    resources "/restaurants", RestaurantController, except: [:new, :edit, :show] do
      resources "/categories", CategoryController, except: [:new, :edit]
      resources "/products", ProductController, except: [:new, :edit]

      resources "/orders", OrderController, except: [:new, :edit] do
        post "/payments", PaymentController, :create
      end
    end

    patch "/mbway/payment-confirmation", PaymentController, :mbway_callback

    get "/utrust/payment-confirmation", PaymentController, :utrust_confirmation_callback
    get "/utrust/payment-cancelation", PaymentController, :utrust_cancelation_callback
    post "/utrust/payment-received", PaymentController, :utrust_received_callback

    get "/payments", PaymentController, :index

    post "/test-payments", PaymentController, :high_test_api
    post "/low-test-payments", PaymentController, :low_test_api

    resources "/ingredients", IngredientController, except: [:new, :edit, :show]
  end
end
