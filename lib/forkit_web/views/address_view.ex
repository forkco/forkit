defmodule ForkitWeb.AddressView do
  use ForkitWeb, :view

  def render("address.json", %{address: address}) do
    %{
      street: address.street,
      city: address.city,
      country: address.country
    }
  end
end
