defmodule ForkitWeb.CategoryView do
  use ForkitWeb, :view
  alias ForkitWeb.{CategoryView, ProductView}

  def render("index.json", %{categories: categories}) do
    %{data: render_many(categories, CategoryView, "category.json")}
  end

  def render("show.json", %{category: category}) do
     %{data: render_one(category, CategoryView, "category.json")}
  end

  def render("category.json", %{category: category}) do
    %{
      id: category.id, 
      name: category.name, 
      image: category.image,
      products: render_many(category.products, ProductView, "product_ingredient.json")
    }
  end
end
