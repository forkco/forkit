defmodule ForkitWeb.MbwayPaymentView do
  use ForkitWeb, :view

  def render("mbway.json", %{mbway_payment: mbway}) do
    %{
      token: mbway.token,
      merchant_id: mbway.merchant_id,
      confirmed: mbway.confirmed
    }
  end
end
