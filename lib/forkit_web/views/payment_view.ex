defmodule ForkitWeb.PaymentView do
  use ForkitWeb, :view
  
  alias ForkitWeb.{PaymentView, MbwayPaymentView}

  def render("index.json", %{payments: payments}) do
    %{data: render_many(payments, PaymentView, "payment.json")}
  end

  def render("payment.json", %{payment: payment}) do
    %{
      order_id: payment.order_id, 
      amount: payment.amount, 
      mbway_payment: render_one(payment.mbway_payment, MbwayPaymentView, "mbway.json"),
    }
  end
end
