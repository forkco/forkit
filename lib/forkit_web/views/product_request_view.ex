defmodule ForkitWeb.ProductRequestView do
  use ForkitWeb, :view
  alias ForkitWeb.{IngredientView, ProductView}

  def render("request.json", %{product_request: request}) do
    %{
      product: render_one(request.product, ProductView, "product.json"),
      note: request.note,
      ingredients: render_many(request.ingredients, IngredientView, "ingredient.json")
    }
  end
end
