defmodule ForkitWeb.ProductView do
  use ForkitWeb, :view
  
  alias ForkitWeb.{ProductView, IngredientView}

  def render("index.json", %{products: products}) do
    %{data: render_many(products, ProductView, "product.json")}
  end

  def render("show.json", %{product: product}) do
    %{data: render_one(product, ProductView, "product.json")}
  end

  def render("product.json", %{product: product}) do
    %{
      id: product.id,
      name: product.name,
      image: product.image,
      price: product.price,
      description: product.description,
      available: product.available
    }
  end

  def render("product_ingredient.json", %{product: product}) do
    %{
      id: product.id,
      name: product.name,
      image: product.image,
      price: product.price,
      description: product.description,
      available: product.available,
      ingredients: render_many(product.ingredients, IngredientView, "ingredient.json")
    }
  end
end
