defmodule ForkitWeb.RestaurantView do
  use ForkitWeb, :view
  
  alias ForkitWeb.{RestaurantView, ScheduleView, AddressView}

  def render("index.json", %{restaurants: restaurants}) do
    %{data: render_many(restaurants, RestaurantView, "restaurant.json")}
  end

  def render("show.json", %{restaurant: restaurant}) do
    %{data: render_one(restaurant, RestaurantView, "restaurant.json")}
  end

  def render("restaurant.json", %{restaurant: restaurant}) do
    %{
      id: restaurant.id, 
      name: restaurant.name, 
      address: render_one(restaurant.address, AddressView, "address.json"),
      schedule: render_one(restaurant.schedule, ScheduleView, "schedule.json")
    }
  end
end
