defmodule ForkitWeb.ScheduleView do
  use ForkitWeb, :view

  def render("schedule.json", %{schedule: schedule}) do
    %{
      open_at: schedule.open_at,
      close_at: schedule.close_at
    }
  end
end
