defmodule Forkit.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string, null: false
      add :image, :string
      add :price, :integer, null: false
      add :description, :string
      add :available, :boolean, default: true, null: false
      add :restaurant_id, references(:restaurants), null: false

      timestamps()
    end

    create unique_index(:products, [:name])
  end
end
