defmodule Forkit.Repo.Migrations.CreateProductsIngredients do
  use Ecto.Migration

  def change do
    create table(:products_ingredients) do
      add :product_id, references(:products), null: false
      add :ingredient_id, references(:ingredients), null: false

      timestamps()
    end
  end
end
