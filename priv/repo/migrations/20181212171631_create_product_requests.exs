defmodule Forkit.Repo.Migrations.CreateProductRequests do
  use Ecto.Migration

  def change do
    create table(:product_requests) do
      add :note, :string
      add :product_id, references(:products), null: false

      timestamps()
    end

    create table(:product_requests_ingredients) do
      add :product_request_id, references(:product_requests), null: false
      add :ingredient_id, references(:ingredients), null: false

      timestamps()
    end
  end
end
