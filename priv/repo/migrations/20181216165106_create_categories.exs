defmodule Forkit.Repo.Migrations.CreateCategories do
  use Ecto.Migration

  def change do
    create table(:categories) do
      add :name, :string, null: false
      add :image, :string
      add :restaurant_id, references(:restaurants), null: false

      timestamps()
    end

    alter table(:products) do
      add :category_id, references(:categories), null: false
    end
  end
end
