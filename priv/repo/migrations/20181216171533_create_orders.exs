defmodule Forkit.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :status, :integer, null: false
      add :type, :integer, null: false
      add :note, :string
      add :restaurant_id, references(:restaurants), null: false

      timestamps()
    end

  end
end
