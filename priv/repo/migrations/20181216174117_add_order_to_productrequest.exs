defmodule Forkit.Repo.Migrations.AddOrderToProductrequest do
  use Ecto.Migration

  def change do
    alter table(:product_requests) do
      add :order_id, references(:orders)
    end
  end
end
