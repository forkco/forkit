defmodule Forkit.Repo.Migrations.CreateSchedules do
  use Ecto.Migration

  def change do
    create table(:schedules) do
      add :open_at, :time, null: false
      add :close_at, :time, null: false

      add :restaurant_id, references(:restaurants), null: false
    end
  end
end
