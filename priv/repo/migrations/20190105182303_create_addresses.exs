defmodule Forkit.Repo.Migrations.CreateAddresses do
  use Ecto.Migration

  def change do
    create table(:addresses) do
      add :street, :string
      add :city, :string
      add :country, :string

      add :restaurant_id, references(:restaurants), null: false
    end
  end
end
