defmodule Forkit.Repo.Migrations.CreatePayments do
  use Ecto.Migration

  def change do
    create table(:payments) do
      add :restaurant_id, :integer, null: false
      add :order_id, :integer, null: false
      add :amount, :integer, null: false

      timestamps()
    end
  end
end
