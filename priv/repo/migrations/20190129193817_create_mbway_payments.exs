defmodule Forkit.Repo.Migrations.CreateMbwayPayments do
  use Ecto.Migration

  def change do
    create table(:mbway_payments) do
      add :token, :string
      add :merchant_id, :integer, null: false
      add :confirmed, :boolean, default: false
      add :payment_id, references(:payments), null: false

      timestamps()
    end
  end
end
