defmodule Forkit.Repo.Migrations.CreateUtrustPayments do
  use Ecto.Migration

  def change do
    create table(:utrust_payments) do
      add :confirmed, :boolean, default: false, null: false
      add :token, :string
      add :return_url, :string
      add :cancel_url, :string
      add :callback_url, :string
      add :payment_id, references(:payments), null: false

      timestamps()
    end

  end
end
