# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Forkit.Repo.insert!(%Forkit.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Forkit.Repo
alias Forkit.Restaurants.{
  Product, 
  Restaurant, 
  Ingredient,
  Category,
  ProductRequest,
  Order, 
  Address,
  Schedule
}

# Restaurants

restaurant1 = %Restaurant{
  name: "PixelsHut",
  address: %Address{
    street: "Pavilhão Carlos Lopes",
    city: "Lisboa",
    country: "Portugal"
  },
  schedule: %Schedule{
    open_at: ~T[08:00:00],
    close_at: ~T[22:00:00]
  }
}

restaurant1 = Repo.insert!(restaurant1)

# Ingredients

ingredient1 = %Ingredient{
  name: "Tomate"
}

ingredient2 = %Ingredient{
  name: "Queijo"
}

ingredient3 = %Ingredient{
  name: "Molho Francesinha"
}

ingredient4 = %Ingredient{
  name: "Massa"
}

ingredient5 = %Ingredient{
  name: "Pão"
}

ingredient6 = %Ingredient{
  name: "Bacon"
}

ingredient7 = %Ingredient{
  name: "Carne de Vaca"
}

ingredient8 = %Ingredient{
  name: "Maionese"
}

ingredient9 = %Ingredient{
  name: "Azeitona"
}

ingredient10 = %Ingredient{
  name: "Cogumelo"
}

ingredient11 = %Ingredient{
  name: "Arroz"
}

ingredient12 = %Ingredient{
  name: "Batata"
}

ingredient13 = %Ingredient{
  name: "Presunto"
}

ingredient14 = %Ingredient{
  name: "Pimento"
}

ingredient15 = %Ingredient{
  name: "Lombo"
}

ingredient1 = Repo.insert!(ingredient1)
ingredient2 = Repo.insert!(ingredient2)
ingredient3 = Repo.insert!(ingredient3)
ingredient4 = Repo.insert!(ingredient4)
ingredient5 = Repo.insert!(ingredient5)
ingredient6 = Repo.insert!(ingredient6)
ingredient7 = Repo.insert!(ingredient7)
ingredient8 = Repo.insert!(ingredient8)
ingredient9 = Repo.insert!(ingredient9)
ingredient10 = Repo.insert!(ingredient10)
ingredient11 = Repo.insert!(ingredient11)
ingredient12 = Repo.insert!(ingredient12)
ingredient13 = Repo.insert!(ingredient13)
ingredient14 = Repo.insert!(ingredient14)
ingredient15 = Repo.insert!(ingredient15)

ingredients_list = []

# Categories
default_category_image = "/images/category.jpg"

category1 = %Category{
  name: "Snacks",
  image: "https://www.filstop.com/images/C/filipino-snacks-01.jpg",
  restaurant: restaurant1
}

category2 = %Category{
  name: "Meals",
  image: "https://img2.goodfon.com/wallpaper/nbig/9/b4/assorti-blyuda-picca-sushi-tort.jpg",
  restaurant: restaurant1
}

category3 = %Category{
  name: "Fruits",
  image: "https://www.1mhealthtips.com/wp-content/uploads/2015/09/low-carb-fruits.jpg",
  restaurant: restaurant1
}

category4 = %Category{
  name: "Drinks",
  image: "https://www.laprimacatering.com/images/Product/large/8050.jpg",
  restaurant: restaurant1
}

category1 = Repo.insert!(category1)
category2 = Repo.insert!(category2)
category3 = Repo.insert!(category3)
category4 = Repo.insert!(category4)


# Products
default_product_image = "/images/product.jpg"

product1 = %Product{
  name: "Redbull",
  image: "https://clubhoreca.ca/wp-content/uploads/2018/10/RED-BULL-ENERGY-355-ML.jpg",
  price: 50,
  description: "The classical energy drink for your code.",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product2 = %Product{
  name: "Redbull Red",
  image: "https://images-na.ssl-images-amazon.com/images/I/81enQLWuCkL._SY679_.jpg",
  price: 50,
  description: "The classical energy drink for your code.",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product3 = %Product{
  name: "Redbull Green",
  image: "https://www.google.com/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjiv5jUvpThAhUDWhoKHdwuDCIQjRx6BAgBEAU&url=https%3A%2F%2Fenergydrink-us.redbull.com%2Fen&psig=AOvVaw1FAbeGgGVGtzU40KLH8DOS&ust=1553300786390858",
  price: 50,
  description: "The classical energy drink for your code.",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product4 = %Product{
  name: "Redbull Yellow",
  image: "https://image.redbull.com/rbx00264/0100/0/406/products/packshots/en_US/USA_355_RB_YELLOW_SingleUnit_closed_cold_front.png",
  price: 50,
  description: "The classical energy drink for your code.",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product5 = %Product{
  name: "Coca-Cola",
  image: "https://www.bigw.com.au/medias/sys_master/images/images/h3c/h24/11473976197150.jpg",
  price: 50,
  description: "The world's favourite coke.",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product6 = %Product{
  name: "Coca-Cola Zero",
  image: "http://goodcart.pk/wp-content/uploads/2018/08/coca-cola-zero.png",
  price: 150,
  description: "The world's favourite coke (for those with a healthy life-style).",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product7 = %Product{
  name: "Fanta",
  image: "https://www.kobster.com/135830-thickbox_default/fanta-can-300ml-pack-of-24.jpg",
  price: 50,
  description: "he nazi Germany alternative to coke.",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product8 = %Product{
  name: "Fanta Maracujá",
  image: "https://images-na.ssl-images-amazon.com/images/I/71c4aCK3M4L._SX522_.jpg",
  price: 50,
  description: "For those of you who hate orange.",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product9 = %Product{
  name: "Water",
  image: "https://5.imimg.com/data5/GV/DP/MY-3831378/500ml-plastic-water-bottle-500x500.jpg",
  price: 50,
  description: "World's oldest soda!",
  available: true,
  restaurant: restaurant1,
  category: category4,
  ingredients: ingredients_list
}

product10 = %Product{
  name: "Popcorn",
  image: "http://pngimg.com/uploads/popcorn/popcorn_PNG32.png",
  price: 100,
  description: "Starts with a \"P\" and ends with an \"orn\"; The perfect companion for a lonely movie night.",
  available: true,
  restaurant: restaurant1,
  category: category1,
  ingredients: ingredients_list
}

product11 = %Product{
  name: "Waffle",
  image: "https://i.dlpng.com/static/png/119354_preview.png",
  price: 300,
  description: "I'm waffled by its taste!",
  available: true,
  restaurant: restaurant1,
  category: category1,
  ingredients: ingredients_list
}

product12 = %Product{
  name: "Maria Cookies",
  image: "https://media.continente.pt/Sonae.eGlobal.Presentation.Web.Media/media.axd?resourceSearchType=2&resource=ProductId=6715842(eCsf$RetekProductCatalog$MegastoreContinenteOnline$Continente)&siteId=1&channelId=1&width=512&height=512&defaultOptions=1",
  price: 100,
  description: "Your grandma's classical.",
  available: true,
  restaurant: restaurant1,
  category: category1,
  ingredients: ingredients_list
}

product13 = %Product{
  name: "Chocolate Cereal Bar",
  image: "https://nit.pt/wp-content/uploads/2017/04/maxresdefault-2-754x394.jpg",
  price: 100,
  description: "The perfect snack for all times.",
  available: true,
  restaurant: restaurant1,
  category: category1,
  ingredients: ingredients_list
}

product14 = %Product{
  name: "Mango Cereal Bar",
  image: "https://nit.pt/wp-content/uploads/2017/04/maxresdefault-2-754x394.jpg",
  price: 100,
  description: "You're Mango Me Crazy",
  available: true,
  restaurant: restaurant1,
  category: category1,
  ingredients: ingredients_list
}

product15 = %Product{
  name: "Chocolate candy",
  image: "https://cdn.albanesecandy.com/candy-store/images/products/popup/chocolate-vanilla-caramels_1.jpg",
  price: 100,
  description: "Chocolate candy",
  available: true,
  restaurant: restaurant1,
  category: category1,
  ingredients: ingredients_list
}

product16 = %Product{
  name: "Noodles",
  image: "https://sgfm.elcorteingles.es/SGFM/dctm/MEDIA02/CONTENIDOS/201509/24/05218089305249____2__600x600.jpg",
  price: 300,
  description: "Everyone's favourite last resource meal.",
  available: true,
  restaurant: restaurant1,
  category: category2,
  ingredients: ingredients_list
}

product17 = %Product{
  name: "Sushi",
  image: "https://cdn77-s3.lazycatkitchen.com/wp-content/uploads/2015/07/vegan-sushi-rolls-800x533.jpg",
  price: 500,
  description: "Raw fish made good.",
  available: true,
  restaurant: restaurant1,
  category: category2,
  ingredients: ingredients_list
}

product18 = %Product{
  name: "Banana",
  image: "https://i5.walmartimages.ca/images/Large/580/6_r/875806_R.jpg",
  price: 100,
  description: "It's more than a snack.",
  available: true,
  restaurant: restaurant1,
  category: category3,
  ingredients: ingredients_list
}

product19 = %Product{
  name: "Apple",
  image: "https://i5.walmartimages.ca/images/Large/428/5_r/6000195494285_R.jpg",
  price: 100,
  description: "A tasty apple (Not poisonous).",
  available: true,
  restaurant: restaurant1,
  category: category3,
  ingredients: ingredients_list
}

product20 = %Product{
  name: "Pear",
  image: "https://www.cmiapples.com/ecom_img/original-15-46-bartlett-pear.jpg",
  price: 100,
  description: "Apple's weird cousin.",
  available: true,
  restaurant: restaurant1,
  category: category3,
  ingredients: ingredients_list
}

product1 = Repo.insert!(product1)
product2 = Repo.insert!(product2)
product3 = Repo.insert!(product3)
product4 = Repo.insert!(product4)
product5 = Repo.insert!(product5)
product6 = Repo.insert!(product6)
product7 = Repo.insert!(product7)
product8 = Repo.insert!(product8)
product9 = Repo.insert!(product9)
product10 = Repo.insert!(product10)
product11 = Repo.insert!(product11)
product12 = Repo.insert!(product12)
product13 = Repo.insert!(product13)
product14 = Repo.insert!(product14)
product15 = Repo.insert!(product15)
product16 = Repo.insert!(product16)
product17 = Repo.insert!(product17)
product18 = Repo.insert!(product18)
product19 = Repo.insert!(product19)
product20 = Repo.insert!(product20)

product_request1 = %ProductRequest{
  product: product1
}

product_request2 = %ProductRequest{
  product: product2
}

product_request3 = %ProductRequest{
  note: "Massa fina",
  product: product3
}

product_request4 = %ProductRequest{
  product: product4,
  ingredients: [ingredient4, ingredient9]
}

product_request1 = Repo.insert!(product_request1)
product_request2 = Repo.insert!(product_request2)
product_request3 = Repo.insert!(product_request3)
product_request4 = Repo.insert!(product_request4)

order1 = %Order{
  restaurant: restaurant1,
  type: 0,
  note: "Estou cá fora",
  requests: [product_request1, product_request4]
}

order2 = %Order{
  restaurant: restaurant1,
  type: 0,
  note: "Se demorar mais de 5 minutos não pago!",
  requests: [product_request2]
}

order3 = %Order{
  restaurant: restaurant1,
  type: 0,
  requests: [product_request3, product_request4, product_request1]
}

Repo.insert!(order1)
Repo.insert!(order2)
Repo.insert!(order3)
