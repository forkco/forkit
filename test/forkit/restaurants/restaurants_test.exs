defmodule Forkit.RestaurantsTest do
  import Forkit.Factory

  use Forkit.DataCase

  alias Forkit.Restaurants
  alias Forkit.Restaurants.{Restaurant, Product, Category}

  describe "restaurants" do
    @valid_attrs params_for(:restaurant)
    @update_attrs params_for(:restaurant)
    @invalid_attrs %{name: nil, address: nil, schedule: nil}

    test "list_restaurants/0 returns all restaurants" do
      restaurant = insert(:restaurant)

      assert Restaurants.list_restaurants() == [restaurant]
    end

    test "get_restaurant!/1 returns the restaurant with given id" do
      restaurant = insert(:restaurant)

      assert Restaurants.get_restaurant!(restaurant.id) == restaurant
    end

    test "create_restaurant/1 with valid data creates a restaurant" do
      assert {:ok, %Restaurant{} = created_restaurant} =
               Restaurants.create_restaurant(@valid_attrs)

      assert created_restaurant.name == @valid_attrs[:name]
    end

    test "create_restaurant/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Restaurants.create_restaurant(@invalid_attrs)
    end

    test "create_restaurant/1 with a duplicate name returns error changeset" do
      assert {:ok, %Restaurant{}} = Restaurants.create_restaurant(@valid_attrs)
      assert {:error, %Ecto.Changeset{} = cs} = Restaurants.create_restaurant(@valid_attrs)

      assert "has already been taken" in errors_on(cs).name
    end

    test "update_restaurant/2 with valid data updates the restaurant" do
      restaurant = insert(:restaurant)

      assert {:ok, %Restaurant{} = restaurant} =
               Restaurants.update_restaurant(restaurant, @update_attrs)

      assert restaurant.name == @update_attrs[:name]
    end

    test "update_restaurant/2 with invalid data returns error changeset" do
      restaurant = insert(:restaurant)

      assert {:error, %Ecto.Changeset{}} =
               Restaurants.update_restaurant(restaurant, @invalid_attrs)

      assert restaurant == Restaurants.get_restaurant!(restaurant.id)
    end

    test "delete_restaurant/1 deletes the restaurant" do
      restaurant = insert(:restaurant)

      assert {:ok, %Restaurant{}} = Restaurants.delete_restaurant(restaurant)

      assert_raise Ecto.NoResultsError, fn -> Restaurants.get_restaurant!(restaurant.id) end
    end
  end

  describe "products" do
    @valid_attrs params_for(:product)
    @update_attrs params_for(:product)
    @invalid_attrs %{available: nil, description: nil, image: nil, name: nil, price: nil}

    def product_fixture do
      restaurant = insert(:restaurant)
      category = insert(:category, restaurant: restaurant)
      product = 
        insert(:product, category: category, restaurant: restaurant)
        |> Repo.preload(:ingredients)

      {product, restaurant}
    end

    test "get_product!/1 returns the product with given id" do
      {product, restaurant} = product_fixture()

      stored_product =
        Restaurants.get_product!(restaurant, product.id)
        |> Repo.preload(category: [restaurant: [:schedule, :address]])
        |> Repo.preload(restaurant: [:schedule, :address])

      assert stored_product == product
    end

    test "create_product/1 with valid data creates a product" do
      restaurant = insert(:restaurant)
      category = insert(:category, restaurant: restaurant)
      valid_attrs = 
        @valid_attrs 
        |> Map.put(:restaurant_id, restaurant.id)
        |> Map.put(:category_id, category.id)

      assert {:ok, %Product{} = product} = Restaurants.create_product(valid_attrs)

      assert product.name == valid_attrs[:name]
      assert product.image == valid_attrs[:image]
      assert product.price == valid_attrs[:price]
      assert product.available == valid_attrs[:available]
      assert product.description == valid_attrs[:description]
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Restaurants.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      {product, _} = product_fixture()

      assert {:ok, %Product{} = product} = Restaurants.update_product(product, @update_attrs)

      assert product.name == @update_attrs[:name]
      assert product.image == @update_attrs[:image]
      assert product.price == @update_attrs[:price]
      assert product.available == @update_attrs[:available]
      assert product.description == @update_attrs[:description]
    end

    test "update_product/2 with invalid data returns error changeset" do
      {product, restaurant} = product_fixture()

      assert {:error, %Ecto.Changeset{}} = Restaurants.update_product(product, @invalid_attrs)

      stored_product =
        Restaurants.get_product!(restaurant, product.id)
        |> Repo.preload(category: [restaurant: [:schedule, :address]])
        |> Repo.preload(restaurant: [:schedule, :address])

      assert product == stored_product
    end

    test "delete_product/1 deletes the product" do
      {product, restaurant}  = product_fixture()

      assert {:ok, %Product{}} = Restaurants.delete_product(product)

      assert_raise Ecto.NoResultsError, fn -> Restaurants.get_product!(restaurant, product.id) end
    end
  end

  describe "ingredients" do
    alias Forkit.Restaurants.Ingredient

    @valid_attrs params_for(:ingredient)
    @update_attrs params_for(:ingredient)
    @invalid_attrs %{name: nil}

    test "list_ingredients/0 returns all ingredients" do
      ingredient = insert(:ingredient)

      assert Restaurants.list_ingredients() == [ingredient]
    end

    test "get_ingredient!/1 returns the ingredient with given id" do
      ingredient = insert(:ingredient)

      assert Restaurants.get_ingredient!(ingredient.id) == ingredient
    end

    test "create_ingredient/1 with valid data creates an ingredient" do
      assert {:ok, %Ingredient{} = created_ingredient} =
        Restaurants.create_ingredient(@valid_attrs)

      assert created_ingredient.name == @valid_attrs[:name]
    end

    test "create_ingredient/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Restaurants.create_ingredient(@invalid_attrs)
    end

    test "create_ingredient/1 with a duplicate name returns error changeset" do
      assert {:ok, %Ingredient{}} = Restaurants.create_ingredient(@valid_attrs)
      assert {:error, %Ecto.Changeset{} = cs} = Restaurants.create_ingredient(@valid_attrs)

      assert "has already been taken" in errors_on(cs).name
    end

    test "update_ingredient/2 with valid data updates the ingredient" do
      ingredient = insert(:ingredient)

      assert {:ok, %Ingredient{} = ingredient} =
        Restaurants.update_ingredient(ingredient, @update_attrs)

      assert ingredient.name == @update_attrs[:name]
    end

    test "update_ingredient/2 with invalid data returns error changeset" do
      ingredient = insert(:ingredient)

      assert {:error, %Ecto.Changeset{}} =
        Restaurants.update_ingredient(ingredient, @invalid_attrs)

      assert ingredient == Restaurants.get_ingredient!(ingredient.id)
    end

    test "delete_ingredient/1 deletes the ingredient" do
      ingredient = insert(:ingredient)

      assert {:ok, %Ingredient{}} = Restaurants.delete_ingredient(ingredient)

      assert_raise Ecto.NoResultsError, fn -> Restaurants.get_ingredient!(ingredient.id) end
    end
  end

  describe "orders" do
    alias Forkit.Restaurants.Order

    @valid_attrs params_for(:order)
    @update_attrs params_for(:order) |> Map.take([:status])
    @invalid_attrs %{note: nil, status: nil, type: nil}

    def order_fixture do
      restaurant = insert(:restaurant)
      order = 
        insert(:order, restaurant: restaurant) 
        |> with_request
        |> Repo.preload([requests: [:product, :ingredients]])

      {restaurant, order}
    end

    test "list_orders/0 returns all orders" do
      {restaurant, order} = order_fixture()

      order_list =
        Restaurants.list_orders(restaurant)
        |> Repo.preload(restaurant: [:schedule, :address])

      assert order_list == [order]
    end

    test "get_order!/1 returns the order with given id" do
      {restaurant, order} = order_fixture()

      stored_order =
        Restaurants.get_order!(restaurant, order.id)
        |> Repo.preload(restaurant: [:schedule, :address])

      assert stored_order == order
    end

    test "create_order/1 with valid data creates a order" do
      restaurant = insert(:restaurant)
      valid_attrs = Map.put(@valid_attrs, :restaurant_id, restaurant.id)

      assert {:ok, %Order{} = order} = Restaurants.create_order(valid_attrs)

      assert order.note == valid_attrs[:note]
      assert order.status == valid_attrs[:status]
      assert order.type == valid_attrs[:type]
    end

    test "create_order/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Restaurants.create_order(@invalid_attrs)
    end

    test "update_order/2 with valid data updates the order" do
      {_restaurant, order} = order_fixture()

      assert {:ok, %Order{} = order} = Restaurants.update_order(order, @update_attrs)

      assert order.status == @update_attrs[:status]
    end

    test "update_order/2 with invalid data returns error changeset" do
      {restaurant, order} = order_fixture()

      assert {:error, %Ecto.Changeset{}} = Restaurants.update_order(order, @invalid_attrs)

      stored_order = Restaurants.get_order!(restaurant, order.id)
                     |> Repo.preload(restaurant: [:schedule, :address])

      assert order == stored_order
    end
  end
  
  describe "categories" do
    @valid_attrs params_for(:category)
    @update_attrs params_for(:category)
    @invalid_attrs %{name: nil}

    def category_fixture() do
      restaurant = insert(:restaurant)
      category = 
        insert(:category, restaurant: restaurant)
        |> Repo.preload(:products)

      {category, restaurant}
    end

    test "list_categories/0 returns all categories" do
      {category, restaurant} = category_fixture()

      categories_list = 
        Restaurants.list_categories(restaurant)
        |> Repo.preload(restaurant: [:schedule, :address]) 

      assert categories_list == [category]
    end

    test "get_category!/1 returns the category with given id" do
      {category, restaurant} = category_fixture()

      stored_category = 
        Restaurants.get_category!(restaurant, category.id)
        |> Repo.preload(restaurant: [:schedule, :address]) 

      assert stored_category == category
    end

    test "create_category/1 with valid data creates a category" do
      restaurant = insert(:restaurant)
      valid_attrs = @valid_attrs |> Map.put(:restaurant_id, restaurant.id)

      assert {:ok, %Category{} = category} = Restaurants.create_category(valid_attrs)
      
      assert category.name == @valid_attrs[:name]
      assert category.image == @valid_attrs[:image]
    end

    test "create_category/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Restaurants.create_category(@invalid_attrs)
    end

    test "update_category/2 with valid data updates the category" do
      {category, _} = category_fixture()

      assert {:ok, %Category{} = category} = Restaurants.update_category(category, @update_attrs)
      
      assert category.name == @update_attrs[:name]
      assert category.image == @update_attrs[:image]
    end

    test "update_category/2 with invalid data returns error changeset" do
      {category, restaurant} = category_fixture()

      assert {:error, %Ecto.Changeset{}} = Restaurants.update_category(category, @invalid_attrs)
      
      stored_category = 
        Restaurants.get_category!(restaurant, category.id)
        |> Repo.preload(restaurant: [:schedule, :address])

      assert stored_category == category
    end

    test "delete_category/1 deletes the category" do
      {category, restaurant}= category_fixture()

      assert {:ok, %Category{}} = Restaurants.delete_category(category)
      assert_raise Ecto.NoResultsError, fn -> Restaurants.get_category!(restaurant, category.id) end
    end
  end
end
