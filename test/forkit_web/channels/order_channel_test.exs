defmodule ForkitWeb.OrderChannelTest do
  use ForkitWeb.ChannelCase

  alias ForkitWeb.OrderChannel

  describe "orders:id" do
    test "broadcasts order update" do
      restaurant = insert(:restaurant)
      %{id: id, status: status} = order = insert(:order, restaurant_id: restaurant.id)

      ForkitWeb.OrderSocket
      |> socket("test_socket", %{})
      |> subscribe_and_join("orders:#{id}")

      OrderChannel.broadcast_order_update(order)

      assert_broadcast("status_updated", %{id: ^id, status: ^status})
    end
  end
end
