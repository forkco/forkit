defmodule ForkitWeb.CategoryControllerTest do
  use ForkitWeb.ConnCase, resource_name: :restaurant_category

  alias Forkit.Restaurants.Category

  @invalid_attrs %{name: nil, image: nil}

  describe "index" do
    setup :create_restaurant

    test "lists all categories", %{conn: conn, restaurant: restaurant} do
      insert(:category, restaurant: restaurant)
      insert(:category, restaurant: restaurant)
      
      response =
        conn
        |> request_index(restaurant)
        |> json_response(200)

      assert length(response["data"]) == 2
    end
  end

  describe "create category" do
    setup :create_restaurant

    test "renders category when data is valid", %{conn: conn, restaurant: restaurant} do
      category = string_params_for(:category) 

      conn = 
        conn
        |> request_create(restaurant, category: category) 
        |> doc
      
      assert %{"id" => id, "products" => products} = json_response(conn, 201)["data"]

      response =
        conn
        |> request_index(restaurant)
        |> json_response(200)

      category = Map.merge(%{"id" => id}, category)
      category = Map.merge(%{"products" => products}, category)

      response["data"]
      |> Enum.member?(category)
      |> assert
    end

    test "renders errors when data is invalid", %{conn: conn, restaurant: restaurant} do
      response =
        conn
        |> request_create(restaurant, category: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "update category" do
    setup :create_category

    test "renders category when data is valid", %{conn: conn, category: %Category{id: id} = category} do
      updated_category = string_params_for(:category)

      conn = 
        conn
        |> request_update(category.restaurant, category, category: updated_category) 
        |> doc
       
      assert %{"id" => ^id, "products" => products} = json_response(conn, 200)["data"]

      response =
        conn
        |> request_index(category.restaurant)
        |> doc
        |> json_response(200)

      updated_category = Map.merge(%{"id" => id}, updated_category)
      updated_category = Map.merge(%{"products" => products}, updated_category)

      response["data"]
      |> Enum.member?(updated_category)
      |> assert
    end

    test "renders errors when data is invalid", %{conn: conn, category: category} do
      response =
        conn
        |> request_update(category.restaurant, category, category: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "delete category" do
    setup :create_category

    test "deletes chosen category", %{conn: conn, category: category} do
      conn = 
        conn
        |> request_delete(category.restaurant, category) 
        |> doc
      
      assert response(conn, 204)

      response =
        conn
        |> request_index(category.restaurant)
        |> doc
        |> json_response(200)
         
      response["data"]
      |> Enum.any?(category)
      |> refute
    end
  end

  defp create_restaurant(_context) do
    restaurant = insert(:restaurant)

    {:ok, restaurant: restaurant}
  end

  defp create_category(_context) do
    restaurant = insert(:restaurant)
    category = insert(:category, restaurant: restaurant)

    {:ok, category: category}
  end
end
