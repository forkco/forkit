defmodule ForkitWeb.IngredientControllerTest do
  use ForkitWeb.ConnCase, resource_name: :ingredient

  alias Forkit.Restaurants.Ingredient

  @invalid_attrs %{name: nil}

  describe "index" do
    test "list all ingredients", %{conn: conn} do
      response = 
        conn 
        |> request_index()
        |> json_response(200)

      assert response["data"] == [] 
    end
  end

  describe "create ingredient" do
    test "renders ingredient when data is valid", %{conn: conn} do
      ingredient = string_params_for(:ingredient)

      conn = request_create(conn, ingredient: ingredient) |> doc
      assert %{"id" => id} = json_response(conn, 201)["data"]

      response = 
        conn
        |> request_index()
        |> doc
        |> json_response(200)

      ingredient = Map.merge(%{"id" => id}, ingredient)
      
      response["data"]
      |> Enum.member?(ingredient)
      |> assert
    end

    test "renders errors when data is invalid", %{conn: conn} do
      response =
        conn
        |> request_create(ingredient: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "update ingredient" do
    setup :create_ingredient

    test "renders ingredient when data is valid", %{
      conn: conn,
      ingredient: %Ingredient{id: id} = ingredient
    } do
      updated_ingredient = string_params_for(:ingredient)

      response =
        conn
        |> request_update(ingredient, ingredient: updated_ingredient)
        |> doc
        |> request_index()
        |> json_response(200)

      updated_ingredient = Map.merge(%{"id" => id}, updated_ingredient)

      response["data"]
      |> Enum.member?(updated_ingredient)
      |> assert
    end

    test "renders errors when data is invalid", %{conn: conn, ingredient: ingredient} do
      response =
        conn
        |> request_update(ingredient, ingredient: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "delete ingredient" do
    setup :create_ingredient

    test "deletes chosen ingredient", %{conn: conn, ingredient: ingredient} do
      conn = request_delete(conn, ingredient) |> doc

      assert response(conn, 204)
    end
  end

  defp create_ingredient(_) do
    ingredient = insert(:ingredient)

    {:ok, ingredient: ingredient}
  end
end
