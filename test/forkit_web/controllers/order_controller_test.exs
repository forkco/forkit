defmodule ForkitWeb.OrderControllerTest do
  use ForkitWeb.ConnCase, resource_name: :restaurant_order

  alias Forkit.Restaurants.Order

  @invalid_attrs %{note: nil, status: 5, type: 3}

  describe "index" do
    test "lists all orders", %{conn: conn} do
      restaurant = insert(:restaurant)

      response =
        conn
        |> request_index(restaurant)
        |> doc
        |> json_response(200)

      assert response["data"] == []
    end
  end

  describe "create order" do
    test "renders order when data is valid", %{conn: conn} do
      restaurant = insert(:restaurant)
      category = insert(:category, restaurant: restaurant)
      product = insert(:product, restaurant: restaurant, category: category)
      order = string_params_with_assocs(:order)
      request = string_params_for(:product_request, [ingredients: [], product: product, order: order])

      order = Map.merge(%{"requests" => [request]}, order)

      conn = request_create(conn, restaurant, order: order) |> doc

      assert %{"id" => id, "requests" => [%{"product" => product}]} = json_response(conn, 201)["data"]

      response =
        conn
        |> request_show(restaurant, id)
        |> json_response(200)

      request = 
        Map.merge(%{"product" => product}, request)
        |> Map.delete("product_id")

      order = Map.replace!(order, "requests", [request])
      assert Map.merge(%{"id" => id}, order) == response["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      restaurant = insert(:restaurant)

      response =
        conn
        |> request_create(restaurant, order: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "update order" do
    setup :create_order

    test "renders order when data is valid", %{conn: conn, order: %Order{id: id} = order} do
      updated_order = string_params_with_assocs(:order, [note: order.note, type: order.type])

      conn =
        conn
        |> request_update(order.restaurant, order, order: updated_order)
        |> doc

      assert %{"id" => ^id, "requests" => requests} = json_response(conn, 200)["data"]

      response =
        conn
        |> request_show(order.restaurant, id)
        |> doc
        |> json_response(200)

      updated_order = Map.merge(%{"id" => id, "requests" => requests}, updated_order)

      assert updated_order == response["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, order: order} do
      response =
        conn
        |> request_update(order.restaurant, order, order: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  defp create_order(_context) do
    restaurant = insert(:restaurant)
    order = insert(:order, restaurant: restaurant) |> with_request

    {:ok, order: order}
  end

end
