defmodule ForkitWeb.ProductControllerTest do
  use ForkitWeb.ConnCase, resource_name: :restaurant_product

  alias Forkit.Restaurants.Product

  @invalid_attrs %{name: nil}

  describe "create product" do
    setup :create_category_restaurant

    test "renders product when data is valid", %{conn: conn, category: category, restaurant: restaurant} do
      product_params = string_params_for(:product) 
      
      product = 
        product_params 
        |> Map.put("restaurant_id", restaurant.id)
        |> Map.put("category_id", category.id)

      conn
      |> request_create(restaurant, product: product) 
      |> doc  
      |> json_response(201)
      |> assert
    end

    test "renders errors when data is invalid", %{conn: conn, restaurant: restaurant} do
      response =
        conn
        |> request_create(restaurant, product: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "update product" do
    setup :create_product

    test "renders product when data is valid", %{conn: conn, product: %Product{} = product} do
      updated_product = string_params_for(:product)

      conn
      |> request_update(product.restaurant, product, product: updated_product) 
      |> doc
      |> json_response(200)
      |> assert
    end

    test "renders errors when data is invalid", %{conn: conn, product: product} do
      response =
        conn
        |> request_update(product.restaurant, product, product: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "delete product" do
    setup :create_product

    test "deletes chosen product", %{conn: conn, product: product} do
      conn =
        conn
        |> request_delete(product.restaurant, product) 
        |> doc

      assert response(conn, 204)
    end
  end

  defp create_category_restaurant(_context) do
    restaurant = insert(:restaurant)
    category = insert(:category, restaurant: restaurant)

    {:ok, category: category, restaurant: restaurant}
  end

  defp create_product(_context) do
    restaurant = insert(:restaurant)
    category = insert(:category, restaurant: restaurant)
    product = insert(:product, category: category, restaurant: restaurant)

    {:ok, product: product}
  end
end
