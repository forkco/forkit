defmodule ForkitWeb.RestaurantControllerTest do
  use ForkitWeb.ConnCase, resource_name: :restaurant

  alias Forkit.Restaurants.Restaurant

  @invalid_attrs %{address: nil, name: nil, schedule: nil}

  describe "index" do
    test "lists all restaurants", %{conn: conn} do
      insert(:restaurant)
      insert(:restaurant)
      
      response =
        conn
        |> request_index()
        |> json_response(200)

      assert length(response["data"]) == 2
    end
  end

  describe "create restaurant" do
    test "renders restaurant when data is valid", %{conn: conn} do
      restaurant = string_params_for(:restaurant)

      conn = request_create(conn, restaurant: restaurant) |> doc
      assert %{"id" => id} = json_response(conn, 201)["data"]

      response =
        conn
        |> request_index()
        |> doc
        |> json_response(200)

      restaurant = Map.merge(%{"id" => id}, restaurant)

      response["data"]
      |> Enum.member?(restaurant)
      |> assert
    end

    test "renders errors when data is invalid", %{conn: conn} do
      response =
        conn
        |> request_create(restaurant: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "update restaurant" do
    setup :create_restaurant

    test "renders restaurant when data is valid", %{
      conn: conn,
      restaurant: %Restaurant{id: id} = restaurant
    } do
      updated_restaurant = string_params_for(:restaurant)

      response =
        conn
        |> request_update(restaurant, restaurant: updated_restaurant)
        |> doc
        |> request_index()
        |> json_response(200)

      updated_restaurant = Map.merge(%{"id" => id}, updated_restaurant)

      response["data"]
      |> Enum.member?(updated_restaurant)
      |> assert
    end

    test "renders errors when data is invalid", %{conn: conn, restaurant: restaurant} do
      response =
        conn
        |> request_update(restaurant, restaurant: @invalid_attrs)
        |> doc
        |> json_response(422)

      assert response["errors"] != %{}
    end
  end

  describe "delete restaurant" do
    setup :create_restaurant

    test "deletes chosen restaurant", %{conn: conn, restaurant: restaurant} do
      conn = request_delete(conn, restaurant) |> doc

      assert response(conn, 204)
    end
  end

  defp create_restaurant(_) do
    restaurant = insert(:restaurant)

    {:ok, restaurant: restaurant}
  end
end
