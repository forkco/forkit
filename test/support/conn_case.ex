defmodule ForkitWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection, specificaly,
  those working with the API endpoints.

  If provided with a :resource_name option, it dynamically
  generates higher level request helper methods

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.

  ## Examples
    use ConnCase, resource_name: :restaurant
    use ConnCase, resource_name: :product
  """

  use Phoenix.ConnTest
  use ExUnit.CaseTemplate

  using(opts) do
    quote do
      # Import conveniences for testing with connections
      use Phoenix.ConnTest

      alias Forkit.Repo

      import Bureaucrat.Helpers
      import ForkitWeb.Router.Helpers
      import Forkit.Factory

      # The default endpoint for testing
      @endpoint ForkitWeb.Endpoint

      ForkitWeb.ConnCase.define_request_helper_methods(unquote(opts))
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Forkit.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Forkit.Repo, {:shared, self()})
    end

    conn =
      %{build_conn() | host: "api."}
      |> put_req_header("accept", "application/json")

    {:ok, conn: conn}
  end

  defmacro define_request_helper_methods(resource_name: resource_name) do
    do_add_request_helper_methods(resource_name)
  end

  defmacro define_request_helper_methods(_), do: nil

  defp do_add_request_helper_methods(resource_name) do
    quote do
      defp default_record() do
        insert(unquote(resource_name))
      end

      defp path_helper_method() do
        "#{unquote(resource_name)}_path"
        |> String.to_atom()
      end

      defp path_for(conn, action, resource_or_id, element_or_id) do
        apply(ForkitWeb.Router.Helpers, path_helper_method(), [
          conn,
          action,
          resource_or_id,
          element_or_id
        ])
      end

      defp path_for(conn, action, resource_or_id) do
        apply(
          ForkitWeb.Router.Helpers,
          path_helper_method(),
          [conn, action, resource_or_id]
        )
      end

      defp path_for(conn, action) do
        apply(ForkitWeb.Router.Helpers, path_helper_method(), [conn, action])
      end

      # INDEX
      def request_index(conn, resource_or_id) do
        path = conn |> path_for(:index, resource_or_id)
        conn |> get(path)
      end

      def request_index(conn) do
        path = conn |> path_for(:index)
        conn |> get(path)
      end

      # SHOW
      def request_show(conn, resource_or_id) do
        path = conn |> path_for(:show, resource_or_id)
        conn |> get(path)
      end

      def request_show(conn, resource_or_id, element_or_id) do
        path = conn |> path_for(:show, resource_or_id, element_or_id)
        conn |> get(path)
      end

      # CREATE
      def request_create(conn, data) do
        path = conn |> path_for(:create)
        conn |> post(path, data)
      end

      def request_create(conn, resource_or_id, data) do
        path = conn |> path_for(:create, resource_or_id)
        conn |> post(path, data)
      end

      # UPDATE
      def request_update(conn, resource_or_id, element_or_id, data) do
        path = conn |> path_for(:update, resource_or_id, element_or_id)
        conn |> put(path, data)
      end

      def request_update(conn, resource_or_id, data) do
        path = conn |> path_for(:update, resource_or_id)
        conn |> put(path, data)
      end

      def request_update(conn, :not_found) do
        request_update(conn, -1, %{})
      end

      def request_update(conn, data) do
        request_update(conn, default_record(), data)
      end

      def request_update(conn) do
        request_update(conn, %{})
      end

      # DELETE
      def request_delete(conn, resource_or_id) do
        path = conn |> path_for(:delete, resource_or_id)
        conn |> delete(path)
      end

      def request_delete(conn, resource_or_id, element_or_id) do
        path = conn |> path_for(:delete, resource_or_id, element_or_id)
        conn |> delete(path)
      end

      def request_delete(conn) do
        request_delete(conn, default_record())
      end
    end
  end
end
