defmodule Forkit.Factory do
  use ExMachina.Ecto, repo: Forkit.Repo

  def product_request_factory do
    %Forkit.Restaurants.ProductRequest{
      note: sequence("Another note")
    }
  end

  def order_factory do
    %Forkit.Restaurants.Order{
      note: sequence("A note"),
      status: sequence(:status, [0, 1, 2]),
      type: sequence(:type, [0, 1])
    }
  end

  def with_request(order) do
    category = insert(:category, restaurant: order.restaurant)
    product = insert(:product, restaurant: order.restaurant, category: category)
    insert(:product_request, product: product, order: order)

    order
  end

  def product_factory do
    %Forkit.Restaurants.Product{
      name: sequence("Product"),
      image: sequence("./path/img"),
      price: sequence(:price, [3, 9, 33, 63, 85, 140]),
      description: sequence("Description"),
      available: sequence(:available, [true, true, false, true])
    }
  end

  def ingredient_factory do
    %Forkit.Restaurants.Ingredient{
      name: sequence("Ingredient")
    }
  end

  def restaurant_factory do
    %Forkit.Restaurants.Restaurant{
      name: sequence("Restaurant"),
      address: %Forkit.Restaurants.Address{
        street: sequence("Street N."),
        city: sequence("City"),
        country: sequence("Country")
      },
      schedule: %Forkit.Restaurants.Schedule{
        open_at: "09:00:00",
        close_at: "20:00:00"
      }
    }
  end

  def category_factory do
    %Forkit.Restaurants.Category{
      name: sequence("Category"),
      image: sequence("./path/img")
    }
  end
end
