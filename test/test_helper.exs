Bureaucrat.start(
  writer: Bureaucrat.ApiBlueprintWriter,
  default_path: "docs/api.md"
)

{:ok, _} = Application.ensure_all_started(:ex_machina)

ExUnit.start(formatters: [ExUnit.CLIFormatter, Bureaucrat.Formatter])
Ecto.Adapters.SQL.Sandbox.mode(Forkit.Repo, :manual)
